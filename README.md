# Weather Crawling

Looking for historical weather data?
You're in the right place!
I've written a simple crawler that collects hourly temperature data for any date you want.

Follow the steps:
- Clone the project.
- Replace the chromedriver depending on your chrome version and OS. [link to download](https://chromedriver.chromium.org/downloads)
- Choose your desired starting and end date in the code.
- Have a look at [Wunderground](https://www.wunderground.com/history) and find the nearest city to your desired city.
- Replace the `driver.get` in the code, depending on your chosen city. e.g., `https://www.wunderground.com/history/daily/jp/narita-shi/RJAA/date/`
- Run the code. It will store the temperature and date in a csv file. 
- You can simply extend the code to store other variables.

This is a really simple code written smartly! I appreciate your contribution to extending it :)

